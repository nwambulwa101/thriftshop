import React from "react";
import { useParams } from "react-router-dom";
// import Product from "../components/Product";
import { clothes } from "../datafiles/clothes";
import "../components/product.css";
import Navbar from "../components/Navbar";
import Slideshow from "../components/Slider";
import "bootstrap/dist/css/bootstrap.min.css";
import Carousel from "../components/ImageGallery";
import ImageGallery from "../components/ImageGallery";

const ProductPage = () => {
  const { id } = useParams();
  const thisProduct = clothes.find(
    (productDetail) => parseInt(productDetail.id) === parseInt(id)
  );

  // useEffect(() => {
  //   console.log(thisProduct.prodname);
  // }, []);

  return (
    <div>
      <Navbar />
      <h3 className="mt-3">{thisProduct?.prodname}</h3>
      <ImageGallery />

      <div className="column">
        <p>This cloth is of size {thisProduct?.size}</p>
        <p className="price">Price: {thisProduct.price}</p>
      </div>
    </div>
  );
};

export default ProductPage;
