import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
// import Product from "../components/Product";
// import { clothes } from "../datafiles/clothes";
import Navbar from "../components/Navbar";
import Banner from "../components/Banner";
import "../components/home.css";
import Advert from "../components/Advert";
import Slideshow from "../components/Slider";
// import Footer from "../components/Footer";

function Home() {
  // const [section, setSection] = useState();

  // const cardElements = clothes.map((cardElement) => {
  //   return (
  //     <Product
  //       image={cardElement.image}
  //       price={cardElement.price}
  //       size={cardElement.size}
  //       prodname={cardElement.prodname}
  //       id={cardElement.id}
  //     />
  //   );
  // });

  // const handleClickScroll = () => {
  //   const element = document.getElementById({ section });
  //   if (element) {
  //     // 👇 Will scroll smoothly to the top of the next section
  //     element.scrollIntoView({ behavior: "smooth" });
  //   }
  // };

  return (
    <div>
      <Navbar />
      {/* <div className="home"> */}
      {/* <Banner /> */}
      <Advert />
      {/* <h1 className="header-newproducts mt-5 mb-3">Categories</h1> */}
      <div className="mt-5 " id="banner">
        <Banner />
      </div>

      {/* <div className="card-container row">{cardElements}</div> */}
      {/* </div> */}
      {/* <div> */}
      {/* <Footer /> */}
      {/* </div> */}
      {/* <Slideshow /> */}
    </div>
  );
}

export default Home;
