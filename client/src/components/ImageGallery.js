import React, { useState } from "react";
import "./ImageGallery.css";
import clothe1 from "../images/clothe4.jpg";
import clothe2 from "../images/clothe2.png";
import clothe4 from "../images/clothe1.png";

const images = [
  { big: clothe1, thumbnail: clothe1 },
  { big: clothe2, thumbnail: clothe2 },
  { big: clothe4, thumbnail: clothe4 },
];

const ImageGallery = (props) => {
  const [selectedImage, setSelectedImage] = useState(images[0].big);

  const handleThumbnailClick = (event) => {
    const newSelection = event.target.dataset.big;
    setSelectedImage(newSelection);
  };

  const thumbnails = images.map((image, index) => (
    <button
      key={index}
      className={
        selectedImage === image.big ? "selected thumbnail" : "thumbnail"
      }
      data-big={image.big}
      onClick={handleThumbnailClick}
      alt=""
    >
      <div
        className="thumbnail-image"
        style={{ backgroundImage: `url(${image.thumbnail})` }}
      ></div>
    </button>
  ));

  return (
    <div className="wrapper">
      <header>
        {/* <h1>{props.}</h1>
        <p>Click on the thumbnail to view it larger on the right!</p> */}
      </header>

      <div className="image-gallery">
        <aside className="thumbnails">{thumbnails}</aside>
        <div>
          <div
            className="image"
            style={{ backgroundImage: `url(${selectedImage})` }}
          ></div>
        </div>
      </div>
    </div>
  );
};

export default ImageGallery;
