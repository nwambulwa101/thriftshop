import React from "react";
import "./product.css";
import { useNavigate } from "react-router-dom";

function Product(props) {
  const navigate = useNavigate();

  const goToProductPage = () => {
    navigate(`/ProductPage/${props.id}`);
  };

  return (
    <div className="card" onClick={goToProductPage} key={props.id}>
      <img src={props.image} alt="" className="images" />
      <div>
        <p className="prodname">{props.prodname}</p>
        <p className="size">Size: {props.size}</p>
        <p className="price">Kes. {props.price}</p>
        <p>{props.id}</p>
      </div>
    </div>
  );
}

export default Product;
