import React from "react";
import img1 from "../images/clothe1.png";
import img2 from "../images/clothe2.png";
import img3 from "../images/clothe4.jpg";
import "./slide.css";
import "bootstrap/dist/css/bootstrap.min.css";

const proprietes = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  arrows: true,
};

const images = [img1, img2, img3];

const Slideshow = () => {
  return (
    <div>
      <div class="container">
        <br />
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
          </ol>

          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img
                src="https://alkite.files.wordpress.com/2009/05/surfing-1.jpg"
                alt="Chania"
                width="460"
                height="345"
              />
              <div class="carousel-caption">
                <h3>Chania</h3>
                <p>
                  The atmosphere in Chania has a touch of Florence and Venice.
                </p>
              </div>
            </div>

            <div class="item">
              <img
                src="https://alkite.files.wordpress.com/2009/05/surfing-1.jpg"
                alt="Chania"
                width="460"
                height="345"
              />
              <div class="carousel-caption">
                <h3>Chania</h3>
                <p>
                  The atmosphere in Chania has a touch of Florence and Venice.
                </p>
              </div>
            </div>

            <div class="item">
              <img
                src="https://alkite.files.wordpress.com/2009/05/surfing-1.jpg"
                alt="Flower"
                width="460"
                height="345"
              />
              <div class="carousel-caption">
                <h3>Flowers</h3>
                <p>Beatiful flowers in Kolymbari, Crete.</p>
              </div>
            </div>

            <div class="item">
              <img
                src="https://alkite.files.wordpress.com/2009/05/surfing-1.jpg"
                alt="Flower"
                width="460"
                height="345"
              />
              <div class="carousel-caption">
                <h3>Flowers</h3>
                <p>Beatiful flowers in Kolymbari, Crete.</p>
              </div>
            </div>
          </div>

          <a
            class="left carousel-control"
            href="#myCarousel"
            role="button"
            data-slide="prev"
          >
            <span
              class="glyphicon glyphicon-chevron-left"
              aria-hidden="true"
            ></span>
            <span class="sr-only">Previous</span>
          </a>
          <a
            class="right carousel-control"
            href="#myCarousel"
            role="button"
            data-slide="next"
          >
            <span
              class="glyphicon glyphicon-chevron-right"
              aria-hidden="true"
            ></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  );
};

export default Slideshow;
