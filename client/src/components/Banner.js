import React from "react";
// import bannerimage from "../images/bannerimage.jpg";
import "./banner.css";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import Product from "../components/Product";
import { clothes } from "../datafiles/clothes";
import TabContext from "@mui/lab/TabContext";
// import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";

function Banner(props) {
  const [value, setValue] = React.useState("one");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const cardElements = clothes.map((cardElement) => {
    return (
      <Product
        image={cardElement.image}
        price={cardElement.price}
        size={cardElement.size}
        prodname={cardElement.prodname}
        id={cardElement.id}
      />
    );
  });

  return (
    <div>
      <TabContext value={value}>
        <Box sx={{ width: "100%" }}>
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="wrapped label tabs example"
          >
            <Tab value="one" label="All Products" wrapped></Tab>
            <Tab value="two" label="New  Arrivals" />
            <Tab value="three" label="Discounted" />
          </Tabs>
        </Box>
        <TabPanel value="one">
          <div className="card-container row" key={cardElements.id}>
            {cardElements}
          </div>
        </TabPanel>
        <TabPanel value="two">Item Two</TabPanel>
        <TabPanel value="three">Item Three</TabPanel>
      </TabContext>

      {/* <div
        className="float-container d-flex justify-content-center mt-5"
        style={{ alignContent: "middle" }}
      >
        <div className="float-child">
          <h1>Thrift Away!</h1>
          <p>
            lorem ipsum dolor sit amet, consectetur adipiscing, lorem ipsum
            dolor.
          </p>
          <button className="btn btn-outline-warning">Start Thrifting</button>
        </div>
        <div className="float-child">
          <img
            src="https://images.pexels.com/photos/2249249/pexels-photo-2249249.jpeg?auto=compress&cs=tinysrgb&w=1600"
            alt=""
            className="image"
          />
        </div>
      </div> */}
    </div>
  );
}

export default Banner;
