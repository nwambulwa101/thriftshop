import React from "react";
import bannerimage from "../images/banner2.jpg";

function Advert() {
  return (
    <div>
      <header style={{ paddingLeft: 0 }}>
        <div
          className="p-5 text-center bg-image"
          style={{
            backgroundImage: `url(${bannerimage})`,
            height: 300,
          }}
        >
          <div
            className="mask"
            style={{
              backgroundColor: "rgba(0, 0, 0, 0.0)",
            }}
          >
            <div className="d-flex justify-content-center align-items-center h-100">
              <div className="text-light">
                <h1 className="mb-3">Lukela's Online Thrift Store</h1>
                <h4 className="mb-3">Bag It All</h4>
                <a
                  className="btn btn-outline-light btn-lg text-light mt-4"
                  href="#!"
                  role="button"
                >
                  Start Thrifting
                </a>
              </div>
            </div>
          </div>
        </div>
      </header>
      {/* <div className="heading mt-5">
        <h1 className="mb-3">Explore Our Range of Products</h1>
      </div>
      <div className="">
        <type className="item">
          <img
            src="https://images.pexels.com/photos/1148957/pexels-photo-1148957.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
            alt=""
          />
          <h1 className="inset-text">Dresses & Gowns</h1>
        </type>
        <img
          src="https://images.pexels.com/photos/1416377/pexels-photo-1416377.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
          alt=""
          className="item"
        />
        <img
          src="https://images.pexels.com/photos/4046313/pexels-photo-4046313.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
          alt=""
          className="item"
        />
        {/* <img
          src="https://images.pexels.com/photos/15589780/pexels-photo-15589780.jpeg?auto=compress&cs=tinysrgb&w=1600"
          alt=""
          className="item"
        /> */}
      {/* </div> */}
    </div>
  );
}

export default Advert;
